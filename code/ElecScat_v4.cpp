#include<iostream>
#include<cmath>
#include"Random64.h"

using namespace std;

const double R0[3]={0.,0.,0.}; //Initial position
const double Cos[3]={0.866,0.,0.5};//Initial direction
const double E0=20.;           //Initial energy in keV
const double Na=6.0221409e+23; //Avogadro's number
const int Nsteps=100;
const int Ne=250;
const double Emin=0.5;         //Cutoff energy in keV
const bool Cutoff=true;

/*
//Gold
const double Z=79.;            //Atomic number of target
const double A=196.96655;      //Atomic weight of target [g/mole]
const double Rho=19.30;        //Target density [g/cm^3]
*/
//Copper
const double Z=29.;            //Atomic number of target
const double A=63.546;         //Atomic weight of target [g/mole]
const double Rho=7.6;          //Target density [g/cm^3]
/*
//Carbon
const double Z=6.;             //Atomic number of target
const double A=12.011;         //Atomic weight of target [g/mole]
const double Rho=2.;           //Target density [g/cm^3]

//Silver
const double Z=47.;            //Atomic number of target
const double A=107.8682;       //Atomic weight of target [g/mole]
const double Rho=10.49;        //Target density [g/cm^3]
*/
const double Z67=pow(Z,0.67);
const double J=1e-3*(9.76*Z+(58.5*pow(Z,-0.19))); //Mean ionization potential [keV]

class Electron{
private:
  double Energy, StepLenght;
  double x,y,z;
  double cx,cy,cz;
  double ca,cb,cc;
public:
  void Init(double x0, double y0, double z0, double cx0, double cy0, double cz0, double E0);
  void Move(double RanA, double RanB, double RanC);
  void PrintPosition();
  void DoStep(double RanA, double RanB, double RanC);
  double Step(double Ran2);
  double Lambda();
  double SigmaE(); // in cm^2
  double Alpha();
  double CosScatAng(double Ran1);
  double dE_dS(); // in keV*cm/g
  void Update();
  double GetEnergy(){return Energy;};
  double GetZ(){return z;};
};
void Electron::Init(double x0, double y0, double z0, double cx0, double cy0, double cz0, double E0){
  x=x0; y=y0; z=z0; Energy=E0; cx=cx0; cy=cy0; cz=cz0;
}
void Electron::PrintPosition(){
  cout<<x<<"\t"<<y<<"\t"<<z<<endl;
}
void Electron::DoStep(double RanA, double RanB, double RanC){
  Move(RanA,RanB,RanC);
  Update();
}
void Electron::Move(double RanA, double RanB, double RanC){
  double Psi,c_Phi;
  StepLenght=Step(RanA);
  Psi=2*M_PI*RanB;
  c_Phi=CosScatAng(RanC);
  
  double AN,AM,V1,V2,V3,V4;
  AM=-(cx/cz);
  AN=pow(1.+(AM*AM),-0.5);
  V1=AN*sqrt(1-(c_Phi*c_Phi));
  V2=AN*AM*sqrt(1-(c_Phi*c_Phi));
  V3=cos(Psi);
  V4=sin(Psi);

  ca=(cx*c_Phi)+(V1*V3)+(cy*V2*V4);
  cb=(cy*c_Phi)+(V4*(cz*V1-cx*V2));
  cc=(cz*c_Phi)+(V2*V3)-(cy*V1*V4);

  x+=StepLenght*ca;
  y+=StepLenght*cb;
  z+=StepLenght*cc;
}
double Electron::Step(double RanA){
  return -Lambda()*log(RanA);
}
double Electron::Lambda(){
  return A/(Na*Rho*SigmaE());
}
double Electron::SigmaE(){
  return 5.21e-21*pow(Z/Energy,2)*(4*M_PI/(Alpha()*(1+Alpha())))*pow((Energy+511.)/(Energy+1024.),2);
}
double Electron::Alpha(){
  return 3.4e-3*Z67/Energy;
}
double Electron::CosScatAng(double RanC){
  return 1.-(2.*Alpha()*RanC)/(1.+Alpha()-RanC);
}
double Electron::dE_dS(){
  return 78500*(Z/(A*Energy))*log(1.166*(Energy+0.85*J)/J);
}
void Electron::Update(){
  Energy-=StepLenght*Rho*dE_dS();
  cx=ca; cy=cb; cz=cc;
}

int main(){
  Crandom ran(1);
  double R1=ran.r(), R2=ran.r(), R3=ran.r();
  double RND1, RND2, RND3;
  Electron Sample[Ne];
  int i,j;


  for(i=0;i<Ne;++i){
    //Initial step
    Sample[i].Init(R0[0],R0[1],R0[2],Cos[0],Cos[1],Cos[2],E0);
    Sample[i].PrintPosition();
    Sample[i].Move(R1,R2,R3);

    if(Cutoff==true){
      //Bunch of steps with Cutoff
      while(Sample[i].GetEnergy()>Emin){
	RND1=ran.r(), RND2=ran.r(), RND3=ran.r();
	Sample[i].DoStep(RND1,RND2,RND3);
	Sample[i].PrintPosition();
	if(Sample[i].GetZ()<0)
	  break;
      }
    }
    else{
      //Bunch of steps
      for(j=0;j<Nsteps;++j){
	RND1=ran.r(), RND2=ran.r(), RND3=ran.r();
	Sample[i].DoStep(RND1,RND2,RND3);
	Sample[i].PrintPosition();
	if(Sample[i].GetZ()<0 || Sample[i].GetZ()>1e-4)
	  break;
      }
    }
    cout << endl;
  }
  return 0;
}
