#include<iostream>
#include<cmath>
#include"Random64.h"

using namespace std;

const double R0[3]={0.,0.,0.}; //Initial position
const double E0=100.;          //Initial energy in keV
const double Na=6.0221409e+23; //Avogadro's number
const int Nsteps=100;
/*
//Gold
const double Z=79.;            //Atomic number of target
const double Z67=pow(Z,0.67);
const double A=196.96655;      //Atomic weight of target [g/mole]
const double Rho=19.30;        //Target density [g/cm^3]
const double J=1e-3*(9.76*Z+(58.5*pow(Z,-0.19))); //Mean ionization potential [keV]
*/
//Copper
const double Z=29.;            //Atomic number of target
const double Z67=pow(Z,0.67);
const double A=63.546;         //Atomic weight of target [g/mole]
const double Rho=7.6;          //Target density [g/cm^3]
const double J=1e-3*(9.76*Z+(58.5*pow(Z,-0.19))); //Mean ionization potential [keV]



double Step(double E, double Ran1);
double Lambda(double E);
double SigmaE(double E);
double Alpha(double E);
double CosScatAng(double E, double Ran1);
double dE_dS(double E);
double norm(double x, double y, double z);


double norm(double x, double y, double z){
  return sqrt(x*x+y*y+z*z);
}

double Step(double E, double Ran1){
  return -Lambda(E)*log(Ran1);
}

double Lambda(double E){ //in cm
  return A/(Na*Rho*SigmaE(E));
}

double SigmaE(double E){ // in cm^2
  return 5.21e-21*pow(Z/E,2)*(4*M_PI/(Alpha(E)*(1+Alpha(E))))*pow((E+511.)/(E+1024.),2);
}

double Alpha(double E){
  return 3.4e-3*Z67/E;
}

double CosScatAng(double E, double Ran1){
  return 1-(2*Alpha(E)*Ran1)/(1+Alpha(E)-Ran1);
}

double dE_dS(double E){ // in keV*cm/g
  return 78500*(Z/(A*E))*log(1.166*(E+0.85*J)/J);
}

int main(){
  Crandom ran(3);
  double Ene=E0;
  double R[3];
  double Phi,Psi;
  int i;
  double c;
  double StepLenght;
  
  R[0]=R0[0];
  R[1]=R0[1];
  R[2]=R0[2];
  StepLenght=Step(Ene,ran.r());
  R[2]+=StepLenght;
  cout<<R[0]<<"\t"<<R[1]<<"\t"<<R[2]<<"\t"<<0<<"\t"<<Ene<<endl;
  
  for(i=1;i<Nsteps+1;++i){
    
    Psi=2*M_PI*ran.r();
    Phi=acos(CosScatAng(Ene,ran.r()));
    StepLenght=Step(Ene,ran.r());
    
    R[0]+=StepLenght*sin(Phi)*cos(Psi);
    R[1]+=StepLenght*sin(Phi)*sin(Psi);
    R[2]+=StepLenght*cos(Phi);

    Ene-=StepLenght*Rho*dE_dS(Ene);
    cout<<R[0]<<"\t"<<R[1]<<"\t"<<R[2]<<"\t"<<i<<"\t"<<Ene<<endl;
  }
  

  return 0;
}
