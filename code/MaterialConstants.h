#include"Material.h"

//-------- XX ---------------(    Z,                A,            Rho);
//---------------------------(     ,         [g/mole],       [g/cm^3]);
Material *  C = new Material (   6.,           12.011,             2.);
Material * Na = new Material (  11.,              23.,          0.971);
Material * Si = new Material (  14.,           28.085,           2.33);
Material *  K = new Material (  19.,             39.1,          0.862);
Material *  V = new Material (  23.,          50.9415,           6.11);
Material * Fe = new Material (  26.,           55.845,          7.874);
Material * Cu = new Material (  29.,           63.546,            7.6);
Material * As = new Material (  33.,         74.92159,          5.776);
Material * Nb = new Material (  41.,         92.90637,           8.57);
Material * Ag = new Material (  47.,         107.8682,          10.49);
Material * Sn = new Material (  50.,          118.710,          7.287);
Material * Cs = new Material (  55.,         132.9054,          1.873);
Material * Eu = new Material (  63.,          151.964,          5.243);
Material * Yb = new Material (  70.,          173.045,          6.965);
Material * Au = new Material (  79.,        196.96655,          19.30);
Material * Hg = new Material (  80.,          200.592,          13.53);


double Z_PMMA = pow(5*6 + 2*8 + 8*1, 1./3.); // C_5 O_2 H_8
double A_H=1.008, A_O=15.999, A_C=12.011;
double A_PMMA = (0.080538*A_H + 0.599848*A_C + 0.319614*A_O);
Material * PMMA = new Material (Z_PMMA,        A_PMMA,           1.18);

