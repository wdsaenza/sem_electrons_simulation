class Material{
private:
  double Z, A, Rho;
public:
 Material(double z, double a, double rho) : Z(z), A(a), Rho(rho) {}
  double GetZ(){return Z;};
  double GetA(){return A;};
  double GetRho(){return Rho;};
};

  

