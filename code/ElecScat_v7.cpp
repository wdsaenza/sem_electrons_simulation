#include<iostream>
#include<cmath>
#include"Random64.h"
#include"MaterialConstants.h"

using namespace std;

const double R0[3]={0.,0.,0.}; //Initial position
const double TiltAng=45.;
const double CompTiltAng=90.-TiltAng;
const double Cos[3]={0.,cos(CompTiltAng*M_PI/180.),cos(TiltAng*M_PI/180.)};
const double E0=30.;           //Initial energy in keV
const double NA=6.0221409e+23; //Avogadro's number
const int Nsteps=100;
const int Ne=250;
const double Emin=0.5;         //Cutoff energy in keV

const bool Cutoff=true;
const bool PrintPosition=true;
const bool PrintSpectra=false;
const bool PrintNoBSE=false;

const double Z  =PMMA->GetZ();
const double A  =PMMA->GetA();
const double Rho=PMMA->GetRho();
const double Zf  =Au->GetZ();
const double Af  =Au->GetA();
const double Rhof=Au->GetRho();

const double Z67=pow(Z,0.67);
const double Zf67=pow(Zf,0.67);
const double J=0.074;
const double Jf=1e-3*(9.76*Zf+(58.5*pow(Zf,-0.19)));


class Electron{
private:
  double Energy, StepLenght;
  double x,y,z;
  double x_old,y_old,z_old;
  double cx,cy,cz;
  double ca,cb,cc;
public:
  void Init(double x0, double y0, double z0, double cx0, double cy0, double cz0, double E0);
  void Move(double RanA, double RanB, double RanC);
  void PrintPosition();
  void DoStep(double RanA, double RanB, double RanC);
  double Step(double Ran2);
  double Lambda();
  double SigmaE(); // in cm^2
  double Alpha();
  double CosScatAng(double Ran1);
  double dE_dS(); // in keV*cm/g
  void Update();
  double GetEnergy(){return Energy;};
  double GetZ(){return z;};
  double GetPhiScat();
  double GetThetaScat();
};
void Electron::Init(double x0, double y0, double z0, double cx0, double cy0, double cz0, double E0){
  x=x0; y=y0; z=z0; Energy=E0; cx=cx0; cy=cy0; cz=cz0; 
}
void Electron::PrintPosition(){
  cout<<x<<"\t"<<y<<"\t"<<z<<"\t"<<Energy<<endl;
}
void Electron::DoStep(double RanA, double RanB, double RanC){
  Move(RanA,RanB,RanC);
  Update();
}
void Electron::Move(double RanA, double RanB, double RanC){
  double Psi,c_Phi;
  double AN,AM,V1,V2,V3,V4;
  if(z>=0){
    x_old=x, y_old=y, z_old=z;
    StepLenght=Step(RanA);
    Psi=2*M_PI*RanB;
    c_Phi=CosScatAng(RanC);
    
    AM=-(cx/cz);
    AN=pow(1.+(AM*AM),-0.5);
    V1=AN*sqrt(1-(c_Phi*c_Phi));
    V2=AN*AM*sqrt(1-(c_Phi*c_Phi));
    V3=cos(Psi);
    V4=sin(Psi);
    
    ca=(cx*c_Phi)+(V1*V3)+(cy*V2*V4);
    cb=(cy*c_Phi)+(V4*(cz*V1-cx*V2));
    cc=(cz*c_Phi)+(V2*V3)-(cy*V1*V4);
    
    x+=StepLenght*ca;
    y+=StepLenght*cb;
    z+=StepLenght*cc;
  }
  else{
    x+=(x-x_old);
    y+=(y-y_old);
    z+=(z-z_old);
  }
}
double Electron::Step(double RanA){
  return -Lambda()*log(RanA);
}
double Electron::Lambda(){
  if(z<2e-6 && z>0)
    return Af/(NA*Rhof*SigmaE());
  else
    return A/(NA*Rho*SigmaE());
}
double Electron::SigmaE(){
  if(z<2e-6 && z>0)
    return 5.21e-21*pow(Zf/Energy,2)*(4*M_PI/(Alpha()*(1+Alpha())))*pow((Energy+511.)/(Energy+1024.),2);
  else
    return 5.21e-21*pow(Z/Energy,2)*(4*M_PI/(Alpha()*(1+Alpha())))*pow((Energy+511.)/(Energy+1024.),2);
}
double Electron::Alpha(){
  if(z<2e-6 && z>0)
    return 3.4e-3*Zf67/Energy;
  else
    return 3.4e-3*Z67/Energy;
  }
double Electron::CosScatAng(double RanC){
  return 1.-(2.*Alpha()*RanC)/(1.+Alpha()-RanC);
}
double Electron::dE_dS(){
  if(z<2e-6 && z>0)
    return 78500*(Zf/(Af*Energy))*log(1.166*(Energy+0.85*Jf)/Jf);
  else
    return 78500*(Z/(A*Energy))*log(1.166*(Energy+0.85*J)/J);
}
void Electron::Update(){
  if(z<2e-6 && z>0)
    Energy-=StepLenght*Rhof*dE_dS();
  else
    Energy-=StepLenght*Rho*dE_dS();
  cx=ca; cy=cb; cz=cc;
}
double Electron::GetPhiScat(){
  double X=x-x_old, Y=y-y_old, Z=z-z_old;
  return acos(abs(Z)*pow(X*X + Y*Y + Z*Z,-0.5));
}
double Electron::GetThetaScat(){
  double X=x-x_old, Y=y-y_old;
  return atan(Y/X);
}


int main(){  
  Crandom ran(1);
  int i,j;
  int NoBSE=0;
  double R1=ran.r(), R2=ran.r(), R3=ran.r();
  double RND1, RND2, RND3;
  Electron * Sample[Ne];
  for(i=0;i<Ne;i++)
    Sample[i] = new Electron;
  //-----Spectra variables-----------//
  int k;
  int Nbin=100;
  int Spec[Nbin];
  for(k=0;k<Nbin;k++)
    Spec[k]=0;
  double Xmax=E0;
  double Xmin=0.;
  double DX=(Xmax-Xmin)/double(Nbin);
  //---------------------------------//
  for(i=0;i<Ne;++i){
    //Initial step
    Sample[i]->Init(R0[0],R0[1],R0[2],Cos[0],Cos[1],Cos[2],E0);
    if(PrintPosition)
      Sample[i]->PrintPosition();
    Sample[i]->Move(R1,R2,R3);
    
    if(Cutoff==true){
      //Bunch of steps with Cutoff
      while(Sample[i]->GetEnergy()>Emin){
	RND1=ran.r(), RND2=ran.r(), RND3=ran.r();
	Sample[i]->DoStep(RND1,RND2,RND3);
	if(PrintPosition)
	  Sample[i]->PrintPosition();
	if(Sample[i]->GetZ()<-3e-4){
	  NoBSE+=1;
	  if(PrintSpectra){
	    for(k=0;k<Nbin;k++)
	      if( Sample[i]->GetEnergy()>double(k)*DX && Sample[i]->GetEnergy()<=double(k+1)*DX){
		Spec[k]+=1;
		break;
	      }
	  }
	  break;
	}
      }
    }
    else{
      //Bunch of steps -- with thickness
      for(j=0;j<Nsteps;++j){
	RND1=ran.r(), RND2=ran.r(), RND3=ran.r();
	Sample[i]->DoStep(RND1,RND2,RND3);
	if(PrintPosition)
	  Sample[i]->PrintPosition();
	if(Sample[i]->GetZ()<0 || Sample[i]->GetZ()>1e-4)
	  break;
      }
    }
    if(PrintPosition)
      cout << endl;
  }
  //---------------Print Spectra n(E)----------------------//
  if(PrintSpectra)
    for(i=0;i<Nbin;i++)
      cout << double(i)*DX + 0.5*DX << "\t" << double(Spec[i])/Ne << endl;
  //---------------Print BSE coefficient-------------------//
  if(PrintNoBSE)
  cout << double(NoBSE)/double(Ne) << endl;
  //-------------------------------------------------------//
  for(i=0;i<Ne;i++)
    delete Sample[i];
  return 0;
}
