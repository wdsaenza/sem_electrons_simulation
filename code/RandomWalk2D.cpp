#include<iostream>
#include<cmath>
#include"Random64.h"

const double R0[2]={0.,0.}; //Initial position
const double Lp=1.0;
const int Nsteps=1000;

using namespace std;

int main(){
  Crandom ran(1);
  double R[2];
  double A=0., B=(M_PI*0.5)-A;
  double Theta;
  int i;
  
  R[0]=R0[0];
  R[1]=R0[1];

  cout << R[0] <<"\t"<< R[1] <<"\t"<<sqrt(R[0]*R[0]+R[1]*R[1])<<"\t"<<0<<endl;
  for(i=1;i<=Nsteps+1;++i){
    Theta=2*M_PI*ran.r();
    A+=Theta;
    B-=Theta;
    R[0]+=Lp*cos(A);
    R[1]+=Lp*cos(B);  
    cout << R[0] <<"\t"<< R[1] <<"\t"<<sqrt(R[0]*R[0]+R[1]*R[1])<<"\t"<<i<<endl;
  }
  return 0;
}
