#include<iostream>
#include<cmath>
#include"Random64.h"

const double R0[3]={0.,0.,0.}; //Initial position
const double Lp=1.0;
const int Nsteps=1000;

using namespace std;

double norm(double x, double y, double z){
  return sqrt(x*x+y*y+z*z);
}


int main(){
  Crandom ran(1);
  double R[3];
  double Phi,Psi;
  int i;
  
  R[0]=R0[0];
  R[1]=R0[1];
  R[2]=R0[2];

  cout << R[0] <<"\t"<< R[1] <<"\t"<<sqrt(R[0]*R[0]+R[1]*R[1])<<"\t"<<0<<endl;
  
  for(i=1;i<=Nsteps+1;++i){
    
    Psi=2*M_PI*ran.r();
    Phi=M_PI*ran.r();
    
    R[0]+=Lp*sin(Phi)*cos(Psi);
    R[1]+=Lp*sin(Phi)*sin(Psi);
    R[2]+=Lp*cos(Phi);
    
    cout<<R[0]<<"\t"<<R[1]<<"\t"<<R[2]<<"\t"<<norm(R[0],R[1],R[2])<<"\t"<<i<<endl;
  }
  return 0;
}
