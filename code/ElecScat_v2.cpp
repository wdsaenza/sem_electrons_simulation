#include<iostream>
#include<cmath>
#include"Random64.h"

using namespace std;

const double R0[3]={0.,0.,0.}; //Initial position
const double E0=100.;          //Initial energy in keV
const double Na=6.0221409e+23; //Avogadro's number
const int Nsteps=100;
/*
//Gold
const double Z=79.;            //Atomic number of target
const double Z67=pow(Z,0.67);
const double A=196.96655;      //Atomic weight of target [g/mole]
const double Rho=19.30;        //Target density [g/cm^3]
const double J=1e-3*(9.76*Z+(58.5*pow(Z,-0.19))); //Mean ionization potential [keV]
*/
//Copper
const double Z=29.;            //Atomic number of target
const double Z67=pow(Z,0.67);
const double A=63.546;         //Atomic weight of target [g/mole]
const double Rho=7.6;          //Target density [g/cm^3]
const double J=1e-3*(9.76*Z+(58.5*pow(Z,-0.19))); //Mean ionization potential [keV]


class Electron{
private:
  double Energy, StepLenght;
  double x,y,z;
public:
  void Init(double x0, double y0, double z0, double E0);
  void Move(double RanA, double RanB, double RanC);
  void PrintPosition();
  void DoStep(double RanA, double RanB, double RanC);
  double Step(double Ran2);
  double Lambda();
  double SigmaE(); // in cm^2
  double Alpha();
  double CosScatAng(double Ran1);
  double dE_dS(); // in keV*cm/g
  void UpdateEnergy();
};
void Electron::Init(double x0, double y0, double z0, double E0){
  x=x0; y=y0; z=z0; Energy=E0;
}
void Electron::PrintPosition(){
  cout<<x<<"\t"<<y<<"\t"<<z<<endl;
}
void Electron::DoStep(double RanA, double RanB, double RanC){
  Move(RanA,RanB,RanC);
  UpdateEnergy();
}
void Electron::Move(double RanA, double RanB, double RanC){
  double Psi,Phi;
  StepLenght=Step(RanA);
  Psi=2*M_PI*RanB;
  Phi=acos(CosScatAng(RanC));
  
  x+=StepLenght*sin(Phi)*cos(Psi);
  y+=StepLenght*sin(Phi)*sin(Psi);
  z+=StepLenght*cos(Phi);
  //cout<<RanA<<"\t"<<RanB<<"\t"<<RanC<<endl;
}
double Electron::Step(double RanA){
  return -Lambda()*log(RanA);
}
double Electron::Lambda(){
  return A/(Na*Rho*SigmaE());
}
double Electron::SigmaE(){
  return 5.21e-21*pow(Z/Energy,2)*(4*M_PI/(Alpha()*(1+Alpha())))*pow((Energy+511.)/(Energy+1024.),2);
}
double Electron::Alpha(){
  return 3.4e-3*Z67/Energy;
}
double Electron::CosScatAng(double RanC){
  return 1-(2*Alpha()*RanC)/(1+Alpha()-RanC);
}
double Electron::dE_dS(){
  return 78500*(Z/(A*Energy))*log(1.166*(Energy+0.85*J)/J);
}
void Electron::UpdateEnergy(){
  Energy-=StepLenght*Rho*dE_dS();
}

int main(){
  Crandom ran(1);
  double R1=ran.r(), R2=ran.r(), R3=ran.r();
  double RND1, RND2, RND3;
  Electron Sample;
  int i;

  Sample.Init(R0[0],R0[1],R0[2],E0);
  Sample.PrintPosition();
  Sample.Move(R1,R2,R3);
  for(i=0;i<Nsteps;++i){
    RND1=ran.r(), RND2=ran.r(), RND3=ran.r();
    Sample.DoStep(RND1,RND2,RND3);
    Sample.PrintPosition();
  }
  return 0;
}
