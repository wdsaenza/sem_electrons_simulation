unset key
set size ratio -1
set xr [-4e-4:4e-4]
set yr [5e-4:-5e-5]
unset tics

set arrow from 27e-5,4.3e-4 to 32e-5,4.3e-4 heads back noborder
#set label at 25e-5,4.62e-4 '0.5 $\mu m$'
#set label at -3.5e-4,-2e-5 '\small{Carbono}'

#set terminal epslatex size 3.7,2.7 color colortext
#set output 'BulkCarbon.tex'

set term pdfcairo size 3.7,2.7
set output 'Carbon.pdf'

plot for [i=0:249] 'CCutOff_E20_Ne250.dat' u 1:3 every :::i::i w l lc rgb "#995F9EA0" lw 1,\
     0 w l lc 2 lw 1.5

