#pmma with coating
unset key
set size ratio -1
unset xtics
unset ytics

set xr [-1.5e-4:1.5e-4]
set yr [1e-4:-1e-4]
set cbl '\small{Energía (keV)}'
set cbtics ('\small{5}' 5,'\small{0}' 0.3)

set arrow from -8e-5,-8e-5 to 0,0 lw 4 front
set arrow from -1.2e-4,7e-5 to -0.7e-4,7e-5 heads back noborder
set label at -0.95e-4,8e-5 '0.5 $\mu$m' center

set palette defined ( 0 "#000090",\
                      1 "#000fff",\
                      2 "#0090ff",\
                      3 "#0fffee",\
                      4 "#90ff70",\
                      5 "#ffee00",\
                      6 "#ff7000",\
                      7 "#ee0000",\
                      8 "#7f0000")

#set terminal epslatex size 3.7,2.7 color colortext
#set output 'ColorPMMA2.tex'

plot for [i=0:249] 'PMMA_45grad_E05_Ne250-NoCot.dat' u 2:3:4 every :::i::i w l lc palette lw 0.5,\
     0 w l lc 2 lw 1.5


