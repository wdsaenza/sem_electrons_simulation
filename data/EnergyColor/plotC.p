unset key
set size ratio -1

set xr [-4e-4:4e-4]
set yr [5e-4:-5e-5]

unset xtics
unset ytics
set cbl '\small{Energía (keV)}'
set cbtics ('\small{20}' 20,'\small{15}' 15, '\small{10}' 10,'\small{5}' 5,'\small{0}' 0.45)

set arrow from 0,-4.5e-5 to 0,0 lw 4
set arrow from 27e-5,4.3e-4 to 32e-5,4.3e-4 heads back noborder
set label at 25e-5,4.62e-4 '0.5 $\mu$m'

set palette defined ( 0 "#000090",\
                      1 "#000fff",\
                      2 "#0090ff",\
                      3 "#0fffee",\
                      4 "#90ff70",\
                      5 "#ffee00",\
                      6 "#ff7000",\
                      7 "#ee0000",\
                      8 "#7f0000")


#set term pdfcairo size 3.7,2.7
#set output 'CarbonColor.pdf'

set terminal epslatex size 3.7,2.7 color colortext
set output 'ColorCarbon.tex'

plot for [i=0:249] 'C_CutOff_E020_Ne250.dat' u 2:3:4 every :::i::i w l lc palette lw 0.5,\
     0 w l lc 2 lw 1.5

