\documentclass[reprint]{revtex4-1}
\usepackage[utf8]{inputenc}
\usepackage[spanish]{babel}
\usepackage{amsmath,amssymb}
\usepackage{graphicx}
\usepackage{amstext}
\usepackage{color}
\usepackage{subcaption}
\usepackage{soul}
\usepackage{rotating}
\usepackage[export]{adjustbox}
\decimalpoint
\usepackage[colorlinks=true,linkcolor=blue,urlcolor=blue,citecolor=blue]{hyperref}
\definecolor{AzulOscuro}{rgb}{0.,0.,0.7}
%\usepackage{caption}
\captionsetup{width=.45\textwidth, font=footnotesize, labelfont=bf,
  singlelinecheck=off}



\begin{document}



\title{Simulación de la trayectoria de un haz de electrones en un sólido usando el método de Monte Carlo}
%
\author{Miller Murillo..}
\email{mimmurillome@unal.edu.co}
\affiliation{Universidad Nacional de Colombia - Bogot\'a, Facultad de Ciencias, Departamento de F\'isica, Bogot\'a, Colombia}
\author{Daniela Angulo.}
\email{dangulom@unal.edu.co}
\affiliation{Universidad Nacional de Colombia - Bogot\'a, Facultad de Ciencias, Departamento de F\'isica, Bogot\'a, Colombia}
\author{William D. Saenz.}
\email{wdsaenza@unal.edu.co}
\affiliation{Universidad Nacional de Colombia - Bogot\'a, Facultad de Ciencias, Departamento de F\'isica, Bogot\'a, Colombia}



\begin{abstract}
En este informe se muestra la implementación del método Monte Carlo como herramienta para simular las trayectorias de electrones con energías entre 5 keV y 30 keV, al pasar a través de una muestra sólida con distintas características físicas. A partir de los datos simulados se calcula el coeficiente de retrodispersión para diferentes configuraciones del modelo experimental, en algunos casos comparando con expresiones analíticas propuestas en la literatura se comprueba la eficiencia del algoritmo utilizado. Al final, se considera el problema específico de muestras que necesitan de un recubrimiento, contrastando la eficiencia en la retrodispersión entre las muestras recubiertas y las no recubiertas.
\end{abstract}
\maketitle

\section{Introducción}
La microscopía electrónica es una técnica de formación de imágenes basada en la incidencia de un haz de electrones sobre una muestra. 
La interacción de este haz con el material es propiciada por la fuerza de Coulomb entre los electrones del haz y el núcleo y los electrones de los átomos del material, que en un primer caso causará la dispersión elástica del electrón caracterizada por la ausencia de transferencia de energía \cite{Krane}. Este tipo de sucesos son responsables de la producción de los electrones retrodispersados. En otros casos, esta interacción puede desencadenar simultáneamente la desviación de la trayectoria del electrón acompañada por un cambio en su energía, siendo el segundo un evento propio de una dispersión inelástica. Ejemplos de este tipo de dispersión son la ionización del átomo emitiendo rayos-X o un electrón Auger, una colisión con un electrón de valencia causando la producción de un electrón secundario o interacción con la red cristalina del sólido generando vibraciones de los iones alrededor de su posición de equilibrio.
Estas dispersiones elásticas o inelásticas se darán sucesivamente hasta que la energía del electrón se agote, llegue al equilibrio térmico con el medio o simplemente, se salga del sólido.

Es complicado modelar todos estos eventos particularmente para cada electrón porque pueden parecer completamente aleatorios y singulares para cada uno, además de contar con el hecho de que alrededor de $10^{10}$ de estos inciden sobre una muestra. Una propuesta prudente para solucionar esta tarea sería asignar probabilidades a ciertos eventos como obtener un electrón retrodispersado o que este sea transmitido a través del blanco, lo cual constituye un escenario perfecto para el método de Monte Carlo \cite{newbury1}.

Siguiendo esta línea de razonamiento, en este trabajo se propone simular las trayectorias que siguen los electrones de un haz que incide en una muestra de un determinado material. Tomando descripciones promedio que den cuenta de las diferentes causas de dispersión con modelos probabilísticos que serán abordados desde el método de Monte Carlo.  

\section{Marco teórico}
La trayectoria del electrón a través del material estará marcada por una serie de dispersiones sucesivas. De estos eventos no todos resultan de la misma importancia o con igual probabilidad de acontecer y como consecuencia, tres suposiciones son establecidas:
\subsection{Dispersión elástica}
Los cambios en el proceder del electrón, específicamente en el camino que este toma, están determinados solamente por dispersiones de naturaleza elástica. Estas desviaciones en la trayectoria del electrón se dan como el resultado de la atracción eléctrica entre el electrón del haz y el núcleo, y ante todo la repulsión entre el primero y los electrones del átomo.

Matemáticamente, estos procesos se describen mediante la sección eficaz de dispersión de Rutherford que modela el núcleo dispersor como un ente puntual, suficientemente masivo respecto al electrón incidente y considera efectos enteramente clásicos a partir del electromagnetismo no relativista y la mecánica newtoniana. La ecuación que describe esta sección eficaz $\sigma_E$ está dada por \cite{newbury2} 
\begin{equation}
\sigma_E=5.21\times 10^{-21}\frac{Z^2}{E^2}\frac{4\pi}{\alpha(1+\alpha)}\biggl(\frac{E+511}{E+1024}\biggl)^2 
\label{cse}
\end{equation}
donde $E$ es la energía en keV, $Z$ es el número atómico efectivo del blanco y $\alpha$ es un factor de apantallamiento incluido para dar cuenta del efecto de la nube electrónica orbitando alrededor del núcleo igual a 
\[
\alpha=3.4\times 10^{-3} \frac{Z^{0.67}}{E}.
\] 
Atribuir la mayor influencia sobre la distribución espacial de la interacción a las dispersiones elásticas se fundamenta en el hecho de que el orden de las deflexiones a partir de aquellas inelásticas es de $\Delta E/E$, que sería significativa en el caso en que una cantidad apreciable de energía se perdiera en uno de estos eventos. Sin embargo, la probabilidad de que estos cambios considerables de energía ocurran es proporcional a $1/ \Delta E$  lo que se traduce en que apenas unos pocos de ellos desencadenarán desviaciones apreciables en la trayectoria del electrón \cite{egerton80}.

Es prudente presentar la forma angular de la sección eficaz de Rutherford que muestre explícitamente la relación con el ángulo de deflexión $\phi$ relativo a la previa dirección de la trayectoria, esta se escribe como 
\begin{equation}
\sigma'=5.21\times 10^{-21}\frac{Z^2}{E^2}\biggl(\frac{E+511}{E+1024}\biggl)^2 \frac{1}{\biggl(\sin^2\frac{\phi}{2}+\alpha\biggl)^2}.
\label{csa}
\end{equation}

\subsection{Camino libre medio}
La sección eficaz descrita por la ecuación \ref{cse} define el camino libre medio $\lambda$ o distancia promedio recorrida por el electrón antes de sufrir otro evento dispersivo de origen elástico. La expresión matemática que describe la relación entre estas dos cantidades es
\begin{equation}
\lambda =\frac{A}{N_a \rho \sigma_E} \text{  [cm]}
\label{mfp}
\end{equation}  
donde $\rho$ es la densidad de la muestra en $\text{g/cm}^3$, $N_a$ es el número de Avogadro y $A$ es la masa atómica del blanco en $\text{g/mol}$.\\
Como su nombre lo indica, esta cantidad sólo representa un promedio y la distancia real que recorre el electrón varía aleatoriamente entre dispersiones sucesivas. La probabilidad de que se recorra una distancia $s$ determinada depende del camino libre medio $\lambda$ así
\begin{equation}
p(s)=exp(-s/\lambda).
\label{pmfp}
\end{equation} 
\subsection{Relación de Bethe}
Para dar cuenta de la pérdida de energía asociada a los eventos de carácter inelástico se considera que el electrón pierde energía de manera continua de acuerdo con la relación de Bethe \cite{Bethe} sin ignorar los hechos de que el electrón tiene una masa muy pequeña y cuenta con un carácter de partícula idéntica. Dentro de esta relación se promedian muchos efectos como la producción de radiación de frenado (Bremsstrahlung) y aquellas que contribuyen en mayor parte como la producción de ionización a partir de extraer electrones de capas internas. La razón de pérdida de la energía con respecto a la distancia recorrida por el electrón dividida entre la densidad se expresa como 
\begin{equation}
\frac{dE}{dS}=-78500\times \frac{Z}{AE} \times \ln \biggl(\frac{1.166E}{J}\biggl)
\label{bethe}
\end{equation}
donde $E$ es la energía del electrón en keV y $S$ es el producto de la densidad $\rho$ y la distancia recorrida $s$. $J$ tiene unidades de energía y se denomina el potencial promedio de ionización que incluye todos los posibles mecanismos de pérdida de energía. $J$ ha sido medido experimentalmente para varios materiales y sus valores se  ajustan a la ecuación \cite{techniques}
\begin{equation}
J=\biggl[9.76Z+\frac{58.5}{Z^{0.19}}\biggl]\times 10^{-3} \text{  [keV]}.
\label{J}
\end{equation}
La relación \ref{bethe} representa un excelente modelo para energías tales que $E>>J$ pero en los casos que no cumplen esta condición, otras ideas deben ser consultadas. Con el ánimo de cubrir todo el rango de energías del haz de electrones se recurrirá a la ecuación propuesta por Joy y Luo en 1989 \cite{Joy} 
\begin{equation}
\frac{dE}{dS}=-78500\times \frac{Z}{AE} \times ln \biggl(\frac{1.166(E+0.85J)}{J}\biggl).
\label{Joy}
\end{equation}

Adicional a las consideraciones ya expuestas, es relevante incluir una expresión que muestre el resultado del efecto de retrodispersión. La fracción de electrones del haz incidente que son dispersados fuera de la muestra es cuantificada por el coeficiente de retrodispersión $\eta$ definido como \cite{goldstein}
\begin{equation}
\eta=\frac{n_{BSE}}{n_B}
\label{retro}
\end{equation} 
donde $n_B$ y $n_{BSE}$ son el número de electrones disparados y retrodispersados, respectivamente.

\section{Método de Monte Carlo}
A menudo se usan algoritmos computacionales del método Monte Carlo para resolver integrales laboriosas o realizar el muestreo de variables aleatorias que sigan una distribución de probabilidad complicada \cite{libroM}.  
Dentro de los dominios de la física, este método goza de gran popularidad sobre todo en la simulación de fenómenos descritos por la mecánica estadística entre estos, el transporte de neutrones \cite{neutrones}, simulación de LEDs \cite{LED} y celdas solares orgánicas \cite{celdas} y la magnetización con el modelo de Ising \cite{ising}. 
En general, si se quiere generar un conjunto de valores correspondientes a una variable aleatoria $X$ de tal manera que su frecuencia siga una distribución determinada $P(x)$ se sigue un procedimiento que constituye la base del método de Monte Carlo integrado fundamentalmente por:

\begin{enumerate}
\item Calcular la función de distribución acumulada $F(x)$ que describe la probabilidad de que la variable aleatoria en cuestión se encuentre dentro una zona de valores menores o iguales a x.
\[
F(x)=P(X\leq x)=\int_{x_{min}}^x P(y)dy
\] 
\item Elegir aleatoriamente un valor de $F(x)$ y despejar para $x$. Lo particular para la escogencia del valor de $F(x)$ es que se hace dentro de un conjunto de números aleatorios entre 0 y 1 distribuidos de manera uniforme.
\item Encontrar el valor para el cual $F(x)=RND$, siendo $RND$ el número aleatorio, habitualmente no es un proceso analítico y la solución más accesible es construir histogramas buscando ahora el rango de x correspondiente a cada $F(x)$.
\end{enumerate}
En la solución presentada en este documento, el comportamiento del electrón se describirá a partir de los efectos de la dispersión elástica y la ecuación de Bethe que están sometidos a las ecuaciones \ref{csa}, \ref{pmfp} y \ref{Joy}. El método de Monte Carlo se aplicará para \ref{csa} y \ref{pmfp} tal y como fue descrito anteriormente, contando con la buena fortuna de que el despeje para la variable aleatoria en ambas ocasiones resulta ser analítico con los siguientes resultados:
\begin{itemize}
\item Para el camino libre medio y la distancia recorrida por el electrón.
\begin{equation}
RND=1-\exp(-s/ \lambda) \rightarrow s=-\lambda \ln (RND)
\label{mfpr}
\end{equation}
\item Para el ángulo de desviación y la sección eficaz \cite{newbury2}.
\begin{equation}
RND=\int_{\omega} \frac{\sigma'(\phi)}{\sigma_E}d\omega \rightarrow \cos\phi=1-\frac{2\alpha RND}{1+\alpha-RND}
\end{equation}
\end{itemize}

\section{Algoritmo y software}

El algoritmo implementado para ejecutar la simulación se compone de los siguientes pasos:
\begin{enumerate}
\item Calcular la distancia de penetración inicial en la muestra.
\item Obtener la energía inicial del electrón.
\item Determinar las coordenadas iniciales para este paso.
\item Encontrar el camino libre medio para este material y energía.
\item Hallar el ángulo de dispersión.
\item Calcular las nuevas coordenadas con los parámetros nuevos.
\item Determinar la energía final para este paso.
\item Restaurar los valores de las coordenadas y la energía.
\end{enumerate}

Este proceso se repite hasta que el electrón deje el material o alcance un valor menor a la energía de corte.

El código que implementa estos pasos fue escrito en $C\texttt{++}$ y se puede descargar libremente desde el repositorio de este trabajo \cite{repo}. Estamos abiertos a cualquier tipo de comentario o sugerencia para mejorar nuestro código.

\section{Resultados y análisis}

En las simulaciones realizadas para este trabajo se consideró un haz de electrones monoenergético, incidiendo sobre una material ideal (con o sin recubrimiento) de extensión infinita ocupando el volumen $z>0$. A continuación se mostrarán distintas figuras que proyectan sobre un plano las trayectorias seguidas por los electrones una vez entran al material. Aunque en estas figuras solo se dibujan 250 trayectorias, los cálculos se realizan con simulaciones de 50000 de ellas.

Principalmente se estudiará el comportamiento del coeficiente de retrodispersión, definido por la ecuación \ref{retro}, al variar los parámetros de entrada de la simulación. Algunos de estos parámetros son el número atómico ($Z$), el ángulo de inclinación del haz respecto a la normal de la superficie de la muestra ($\delta$) y la energía de incidencia de los electrones ($E_0$).

Un primer ejemplo de las trayectorias de los electrones simuladas se muestra en la Figura \ref{fig:1erEx}, el material de la muestra es carbono ($Z=6$) y la energía inicial de los electrones es 20 keV. En esta figura es posible apreciar la dirección de incidencia, determinada por la flecha negra, y el cambio de la energía del electrón a lo largo de cada trayectoria. 

\begin{figure}[h!]
\vspace{-1cm}
	\input{ColorCarbon}
\centering
\vspace{-1.5cm}
  \caption{Trayectorias de los electrones con $E_0=20$ keV que inciden sobre una muestra de carbono (línea verde horizontal).}
  \label{fig:1erEx}
\end{figure}

El volumen de interacción, es decir el volumen que definen las trayectorias de los electrones en el material hasta alcanzar la energía de corte (0.5 keV), depende fuertemente de la energía del haz. Esto se puede comprobar a partir de los resultados en la Figura \ref{fig:vol-int}. Lo anterior coincide con lo esperado, pues según la ecuación (\ref{bethe}) $dE/ds\sim 1/E^2$ a mayor energía incidente, la disipación energética de los electrones es menor y pueden penetrar más en la muestra.

\begin{figure}[h!]
\includegraphics[scale=0.7]{Cu-dE.pdf}
\centering
\caption{Trayectorias de los electrones al incidir sobre una muestra de cobre con distintas energías iniciales. Los tres casos están a la misma escala. Incidencia normal a la superficie.}
\label{fig:vol-int}
\end{figure}

Otro factor que influye en el tamaño del volumen de interacción es el ángulo de incidencia. En la Figura \ref{fig:vol-ang} se muestran las trayectorias para tres inclinaciones del haz. Se observa una disminución del volumen de interacción para ángulos más grandes (este ángulo de incidencia $\delta$ es medido respecto a la normal de la superficie). Además de lo anterior, se nota que para $\delta\neq 0$ la simetría del volumen se pierde, resultando una mayor acumulación de las trayectorias cerca a la superficie y al lado opuesto del haz. 

\begin{figure}[h!]
\includegraphics[scale=0.7]{Cu-dA.pdf}
\centering
\caption{Trayectorias de los electrones para distintos ángulos de incidencia sobre una muestra de cobre. Los tres casos están a la misma escala.}
\label{fig:vol-ang}
\end{figure}

Más adelante se estudiará este efecto cuantitativamente mediante el conteo de los electrones que logran escapar de la muestra, conocidos como electrones retrodispersados o BSE por sus siglas en inglés (\textit{Back-Scattered Electrons}).

Cuando se realizan simulaciones utilizando distintos materiales, no solo resulta que el volumen de interacción cambia sino que su forma también, incluso cuando todos tienen el mismo ángulo de incidencia. Este resultado se expone en la Figura \ref{fig:all1} para cuatro materiales.

\begin{figure*}[t]
\includegraphics[scale=0.8]{All.pdf}
\centering
\caption{Trayectorias de los electrones en distintos materiales. La incidencia en todos los casos es normal a la superficie y con $E_0=20$ keV. Se observa un incremento del volumen de interacción para los materiales con mayor $Z$.}
\label{fig:all1}
\end{figure*}

Dos aspectos son importantes en este punto, el incremento en el volumen de interacción para muestras con átomos más pesados se justifica a partir de la ecuación (\ref{csa}), en donde $\sigma'\sim Z^2$ y por lo tanto se espera que en átomos como el oro la probabilidad de dispersión sea más grande, generando en promedio una mayor desviación de los electrones. Esto implica que el haz es dispersado rápidamente a partir de su dirección inicial, y la penetración en el material es más difícil.

\subsection{Electrones retrodispersados (BSE)}

Con el objetivo de cuantificar los comportamientos previamente descritos, se estudia el coeficiente de retrodispersión $\eta$ para distintas configuraciones de la simulación. Esta cantidad establece uno de los parámetros decisivos en la microscopia SEM, puesto que está relacionado con el número de electrones que son detectados y que se usan para extraer información de la muestra. De esta forma se plantea que a mayor $\eta$ la configuración es más efectiva.

Durante el cálculo de $\eta$ (ecuación \ref{retro}), resultan valores distintos para simulaciones con los mismos parámetros. Al ser ``experimentos'' independientes, con otras trayectorias aleatorias, el número de electrones que escapan del material cambia en cada simulación. No obstante, si se considera un alto número de electrones incidentes la estadística será mejor y el coeficiente se acercará al valor real. Se encontró para cierto caso particular que $\eta$ varía en $0.9\%$ al pasar de $5\times 10^{2}$ a $5\times 10^{3}$ electrones incidentes, y en un $0.8\%$ al pasar de $5\times 10^{2}$ a $5\times 10^{3}$. Por tal razón los coeficientes expuestos en este trabajo se obtuvieron al simular $5\times 10^{3}$ trayectorias por cada configuración.

\subsubsection{Dependencia del número atómico}

Se mostró anteriormente que el volumen de interacción crece para valores de $Z$ más grandes, sin embargo asociado a cada material hay una densidad y una masa molecular que también intervienen en las ecuaciones implementadas en la simulación. No obstante, tal como lo muestra la Figura \ref{fig:fit}, el comportamiento de $\eta$ es en buena aproximación monótono creciente. Esto representa una posible manera mediante la cual la detección de los BSE revela características del material en estudio. Debido a que las grandes variaciones del coeficiente de retrodispersión se dan para valores pequeños de $Z$, este comportamiento solo resultaría útil para discernir entre elementos en esta región \cite{goldstein}.

\begin{figure}[h!]
	\input{n_Z}
\centering
  \caption{Comportamiento del coeficiente de retrodispersión en función del número atómico. Se muestra el ajuste propuesto por \cite{reuter}: $\eta=-0.04(2)+0.018(2)Z-2.4(4)\times10^{-4}Z^2+1.3(3)\times10^{-7}Z^3$.}
  \label{fig:fit}
\end{figure}

\subsubsection{Dependencia de la energía del haz}

Aunque la Figura \ref{fig:vol-int} podría sugerir un incremento en $\eta$ debido al crecimiento del volumen de interacción, esto no es precisamente lo que resulta de las simulaciones cuando se consideran energías del haz entre 5 keV y 50 keV, tal como se muestra en la Figura \ref{fig:nZE}. Estos resultados concuerdan con los datos obtenidos por Bishop (1966)\cite{bishop} y Heinrich (1966)\cite{heinrich}. 

Totalmente opuesto, se encuentra que $\eta$ es casi independiente de la energía del haz, lo cual se puede entender si se piensa en el siguiente caso: un electrón con el doble de energía ($2E'$) de otro electrón que es absorbido en la frontera del volumen de interacción, intenta volver a la superficie con tan solo la mitad de su energía inicial (pues ha perdido la mitad llegando a este punto), sin embargo esto último es poco probable debido a que al tener menor energía su longitud de penetración es menor y no alcanzaría a recorrer la misma distancia de vuelta a la superficie. De esta forma tanto el electrón de energía $E'$ como el de $2E'$ son absorbidos por el material \cite{goldstein}.

\begin{figure}[h!]
	\input{n_Z-E}
\centering
  \caption{Comportamiento del coeficiente de retrodispersión para diferentes valores de la energía del haz $E_0$, en muestras de distinto $Z$.}
  \label{fig:nZE}
\end{figure}

\subsubsection{Dependencia del ángulo de inclinación del haz}

Se encontró para cuatro materiales (C, Cu, Ag y Au) que el coeficiente de retrodispersión crece monótonamente con el ángulo de incidencia. Aunque cada material tiene un corte distinto en $\delta=0$, todos parecen converger a $\eta=1$ cuando $\delta\rightarrow 90^{\circ}$. Ver Figura \ref{fig:n_tilt}.

Resulta lógico este comportamiento si se imagina el volumen de interacción como un cono cuyo eje está determinado por el haz de electrones. A incidencia normal este cono solo encierra trayectorias dentro de la muestra, mientras que a incidencia oblicua parte de este cono puede estar por encima de la superficie, implicando un alto número de electrones retrodispersados. A medida que el ángulo aumenta, una parte más grande del cono sale sobre la superficie.

\begin{figure}[h!]
	\input{n_tilt}
\centering
  \caption{Distintos ángulos de inclinación.}
  \label{fig:n_tilt}
\end{figure}

Una forma de evaluar este resultado es mediante ajustes de la forma $\eta(\delta)=1/(1+\cos{\delta})^p$, en donde $p=9/\sqrt{Z}$ (esta expresión es propuesta por Arnal \textit{et al.} \cite{arnal}). En la misma figura se muestran los respectivos ajustes en donde se obtiene para el oro $Z_{\text{ajuste}}=103(6)$, la plata $Z_{\text{ajuste}}=56(3)$, el cobre $Z_{\text{ajuste}}=32(2)$ y para el carbono $Z_{\text{ajuste}}=6(1)$. Es decir, el modelo se acerca más a las simulaciones con muestras de $Z$ pequeño.

\subsection{Distribuciones de energía de los BSE}

A medida que los electrones son dispersados dentro del material van perdiendo energía. Por lo tanto, dependiendo del número de dispersiones que sufren aquellos electrones que logran escapar de la muestra, se tiene una distribución de energías particular. Tal distribución resulta importante de estudiar porque proporciona información de las energías más frecuentes con las que salen los electrones del material, permitiendo definir las características del detector que se implementaría en el experimento. 

Hasta el momento, en todas las gráficas solo se han mostrado las trayectorias de los electrones dentro del material. Aquellos que llegan a la superficie y logran escapar continúan una trayectoria recta definida por el ángulo de escape y la energía del electrón en ese momento. La Figura \ref{fig:colorCu} se muestra un ejemplo de estas distribuciones, en donde la mayoría de electrones retrodispersados tienen energías cercanas a $E_0$.  

\begin{figure}[h!]
\vspace{-1cm}
	\input{ColorCobre}
\centering
\vspace{-1.5cm}
  \caption{Trayectorias de los electrones dentro y fuera del material. Las trayectorias rectilíneas situadas sobre el plano de la muestra (cobre para este caso) representan los BSE.}
  \label{fig:colorCu}
\end{figure}

Para un análisis detallado de estas distribuciones resulta conveniente construir espectros de energía de los BSE. Aunque en este trabajo no se hace énfasis en el ángulo de retrodispersión, se propone como una continuación del mismo. Por lo anterior, los espectros (o más exactamente los histogramas) serán llenados con todos los BSE sin importar el ángulo de retrodispersión. La Figura \ref{fig:spectra} muestra el resultado de este procedimiento para cuatro muestras (Au, Ag, Cu y C), simulando $5\times 10^{5}$ electrones incidentes con energía $E_0=20$ keV.

Se observa en general que los espectros siguen una forma continua suave. Para muestras de número atómico $Z>30$, la distribución alcanza un máximo bien definido con energía cercana a la energía de incidencia. A medida que disminuye $Z$, esta forma campanoide se hace más ancha y el máximo se corre a energías menores.

Lo anterior sugiere que la mayoría de electrones retrodispersados en muestras de alto $Z$ corresponden a trayectorias con poca longitud de penetración, dicho en otras palabras, para las muestras que presentan un máximo de energía definido y angosto, los electrones que son dispersados en cercanías de la superficie poco después de incidir sobre la muestra representan la mayoría de los BSE.

A bajo $Z$ (como el carbono) la distribución en energías tiende a ser casi uniforme, lo que implica que tanto los electrones que penetran mucho o poco en la muestra, tienen probabilidades similares de alcanzar la superficie y escapar del material.

\begin{figure}[h!]
	\input{spectra}
\centering
  \caption{Espectro de energía de los BSE. Histogramas construidos a partir de $5\times 10^5$ electrones incidentes.}
  \label{fig:spectra}
\end{figure}

\subsection{Retrodispersión en materiales recubiertos}

A continuación se mostrará el comportamiento de las trayectorias de electrones y el coeficiente de retrodispersión para un material cubierto con una película delgada. Como se vio anteriormente, en muestras de bajo $Z$ como el carbono, el valor de $\eta$ es bajo. Esto en principio dificulta el estudio de estos materiales por este mecanismo, sin embargo existe la posibilidad de recubrir la muestra con un material de alto $Z$. Esta cubierta por lo general es delgada ($\sim 10$ nm), permitiendo que los aspectos morfológicos de la muestra sigan siendo apreciables. 

En la simulación se consideró polimetilmetacrilato (conocido comercialmente como plexiglas o PMMA) recubierto con una capa de oro de 20 nm. Este material cuya fórmula estequiométrica es (C$_5$O$_2$H$_8$)$_n$, presenta características similares a las del carbono y por lo tanto resulta razonable usar el recubrimiento. Debido a que este es un compuesto químico, dentro de la simulación se deben tratar las variables de número atómico y masa molecular como cantidades efectivas. Mientras que el número atómico efectivo está dado por \cite{murty}
\begin{equation}
Z_{ef}= \left(\sum_i \alpha_i Z_i\right)^{1/3},
\end{equation}

en donde los $Z_i$ son los números atómicos de los elementos que forman el compuesto y los $\alpha_i$ son los respectivos contenidos electrónicos, la masa molecular es simplemente el promedio ponderado de cada uno de sus constituyentes \cite{nist}.

Para lograr apreciar de forma gráfica las trayectorias de la simulación con y sin recubrimiento, se hizo incidir el haz con un ángulo de $45^{\circ}$ para energías de 5 keV y 30 keV. Los cuatro casos se muestran en la Figura \ref{fig:all2}. A partir de esta figura, se evidencia la variación del número de BSE y el volumen de interacción respecto a la muestra sin recubrir. A pesar de que para ambas energías del haz, el número de BSE aumenta respecto a la muestra sin recubrir, es a bajas energías de incidencia ($E_0=5$ keV) donde el cambio es drástico. En la Figura \ref{fig:all2}(c) se observa que la película de oro no solo provoca la retrodispersión de electrones con alta energía sino que el volumen de interacción en el PMMA se ve reducido a unos escasos electrones.

Este resultado permite entender cualitativamente el objetivo del recubrimiento en materiales de bajo $Z$, que incluso se puede extender a materiales no conductores en donde el número de electrones retrodispersados también es escaso.

\begin{figure*}[t]
	\input{all-pmma}
\centering
  \caption{Comparación de las trayectorias de los electrones en PMMA con recumbrimiento de oro, $(a)$ con $E_0=30$ keV y $(c)$ con $E_0=5$ keV. En las figuras $(b)$ y $(d)$ se muestra el resultado sin usar el recubrimiento a 30 keV y 5 keV, respectivamente.}
  \label{fig:all2}
\end{figure*}

\subsubsection{Dependencia de $\eta$ con la energía del haz en materiales recubiertos}

Por último, con el objetivo de cuantificar el efecto del recubrimiento, se hace una gráfica del coeficiente de retrodispersión en función de la energía del haz para dos ángulos de incidencia diferentes. En la Figura \ref{fig:compara} se muestra la comparación entre la muestra recubierta y la no recubierta (PMMA y oro una vez más).

Este resultado permite afirmar que los efectos de la película de oro con un grosor de 20 nm, son importantes a bajas energías del haz ($\sim 5$ keV). Si la energía es muy alta, los electrones podrán atravesar completamente la región de oro y se tendrá que el comportamiento está definido principalmente por el PMMA. Esto se cumple de forma equivalente para los dos ángulos contemplados en la Figura \ref{fig:compara}.

\begin{figure}[h!]
	\input{pmma}
\centering
  \caption{Comparación del coeficiente de retrodispersión en función de la energía del haz, entre la muestra de PMMA recubierta con oro y otra sin ningún recubrimiento. Los datos rojos corresponden a $\delta=45^{\circ}$, y los azules a $\delta= 0^{\circ}$}
  \label{fig:compara}
\end{figure}



\section{Conclusiones}
\begin{itemize}

\item Al momento de simular sistemas físicos no es necesario llevar un registro determinista de los efectos de cada interacción. En este caso, por ejemplo, bastó con unas cuantas suposiciones y tres modelos, dispersión de Rutherford, camino libre medio y ecuación de Bethe, para conseguir resultados satisfactorios que describieran de manera fidedigna la trayectoria de un electrón dentro de un sólido. De esta manera, además de simplificar el modelo, se ahorró tiempo de cómputo usando un método estocástico como Monte Carlo. 

\item De acuerdo a los resultados obtenidos, para materiales aislantes o de número atómico pequeño, las condiciones óptimas se dan cuando la energía del haz de electrones es del orden de 5 keV, recubrimiento de 20 nm de espesor e incidencia oblicua. 

\item Usando la relación monótona y creciente entre el coeficiente de retrodispersión y el número atómico es posible caracterizar el material para elementos con Z menor a 40 porque dicha función presenta una variación apreciable mientras que a medida que se sobrepasa este límite, la pendiente tiende a cero y esto complica la identificación de elementos con número atómico similar.

\item Para muestras con Z mayor a 30 los electrones retrodispersados presentan energías comparables con la incidente, dada la fuerte interacción desarrollada dentro del material que impide que estos electrones penetren distancias que causen un cambio considerable en su energía.

\item Aunque el volumen de interacción depende de los parámetros variados durante las simulaciones, estos son energía del haz, ángulo de incidencia y número atómico, se encontró que no sucede lo mismo con el coeficiente de retropdispersión. En cambio, para este último, la influencia de la energía del haz no es un marcador determinante.

\end{itemize}

\begin{thebibliography}{2}
\section{Referencias} 

\bibitem{Krane}
Krane, K. S., \& Halliday, D. (1988). Introductory nuclear physics. New York: Wiley.

\bibitem{newbury1}
Newbury, D. E., \& Myklebust, R. L. (1978). Monte carlo electron trajectory simulation of beam spreading dm thin foil targets. Ultramicroscopy, 3, 391-395. 

\bibitem{newbury2}
Heinrich, K. F., Newbury, D. E., \& Myklebust, R. L. (1981). Energy dispersive x-ray spectrometry.

\bibitem{egerton80}
Egerton, R. F. (1986). Instrumentation for Energy-Loss Spectroscopy. Electron Energy-Loss Spectroscopy in the Electron Microscope, 27-128.

\bibitem{Bethe}
Bethe, H. (1930). Zur Theorie des Durchgangs schneller Korpuskularstrahlen durch Materie. Ann. Phys. Annalen Der Physik, 397(3), 325-400.  

\bibitem{techniques}
Leo, W. R. (1994). Techniques for Nuclear and Particle Physics Experiments. 

\bibitem{Joy}
Joy, D. C., \& Luo, S. (1989). An empirical stopping power relationship for low-energy electrons. Scanning, 11(4), 176-180.

\bibitem{goldstein} 
Goldstein, J. (1981). Scanning electron microscopy and X-ray microanalysis: A text for biologists, materials scientists, and geologists. New York: Plenum Press.

\bibitem{libroM}
Rubinstein, R. Y., \& Kroese, D. P. (2008). Simulation and the monte carlo method. Hoboken, NJ: John Wiley \& Sons. 

\bibitem{neutrones}
N. Metropolis. The beginning of the Monte Carlo method. Los Alamos Science, 15:125–130, 1987.

\bibitem{LED}
M. Mesta, M. Carvelli, R. J. de Vries, H. van  Eersel, J. J. M. van der Holst, M. Schober, M. Furno, B. Lüssem, K. Leo, P. Loebl, R. Coehoorn, \&  P. A. Bobbert. Molecular-scale  simulation of electroluminescence in a multilayer white organic light-emitting diode. Nature Materials, 12:652–658, 2013.

\bibitem{celdas}
O. Stenzel, L. J. A. Koster, R. Thiedmann, S. D. Oosterhout, R. A. J. Janssen, \& V. Schmidt. A new approach to model-based simulation of disordered polymer blend solar cells. Advanced Functional Materials, 22(6):1236–1244, 2012.

\bibitem{ising}
Najafi, M. (2016). Monte Carlo study of the Ising ferromagnet on the site-diluted triangular lattice. Physics Letters A, 380(3), 370-376.

\bibitem{repo}
Angulo D., Saenz W., Murillo M, ``Simulación de la trayectoria de un haz de electrones en un sólido usando el método de Monte Carlo'', (2016), Bitbucket repository, \url{https://bitbucket.org/wdsaenza/sem_electrons_simulation}

\bibitem{reuter} 
Reuter, W. ``Proceedings of the 6th International Conference on X-ray Optics and Microanalysis.'' (1972): 121.

\bibitem{bishop}
Bishop, H. (1966). ``In Proceedings of the 4th International Conference on X-ray Optics and Microanalysis'' (R. Castaing, P. Deschamps, and J. Philibert, eds.), Hermann, Paris, p 153.

\bibitem{heinrich}
Heinrich, K. F. J. (1966). ``In Proceedings of the 4th International Conference on X-ray Optics and Microanalysis'' (R. Castaing, P. Deschamps, and J. Philibert, eds.), Hermann, Paris, p. 159. 

\bibitem{arnal} 
Arnal, F., P. Verdier, and P. D. Vincensini. "Coefficient de retrodiffusion dans de cas d’electrons monocinetique arrivant sur la cible sous une incidende oblique." Compt Rend Acad Sci 268 (1969): 1526-1529.

\bibitem{murty} 
Murty, R. C. ``Effective Atomic Numbers of Heterogeneous Materials.'' Nature 207.4995 (1965): 398-99. Web. 

\bibitem{nist}
NIST. \textit{Composition of POLYMETHYL METHACRALATE (LUCITE, PERSPEX, PLEXIGLASS)}. \url{http://physics.nist.gov/cgi-bin/Star/compos.pl?matno=223}. (Noviembre 2016).



\end{thebibliography}

\end{document}



\end{thebibliography}

\end{document}
